//Activation
(function() {

try {
    var optTestPageType2 = dataLayer[0];
}
catch(err) {
    console.log('no data layer defined');
    var optTestPageType2 = { pageCategory: "null" , pageName : "null"};
}
optTestPageType2.pageName === "Grid: " && jQuery('.product_listing_container .product_info .product_promo').length || optTestPageType2.pageCategory === "Product Page" || optTestPageType2.pageCategory === "Shopping Cart" || optTestPageType2.pageCategory === "CheckoutLoginFormView" || optTestPageType2.pageCategory === "Search Results"


})();

//Variation 1
(function() {
	//Change Promo Text Color

	/* _optimizely_evaluate=force */
	jQuery('head').append('<style type="text/css">.product_listing_container ul.grid li.grid-item .product_description p.product_promo,.product-details .product-promo-description,.qv-product-details-container .qv-promotion-desc,#order_details .promoDescription,.product-promo-description,.product_listing_container .grid-item .product_description .product_promo{color:#046f48 !important;}</style>');   
	/* _optimizely_evaluate=safe */

})();

//All
(function() {

	try {
	    var optTestPageType = dataLayer[0];
	}
	catch(err) {
	    var optTestPageType = { pageCategory: "null" , pageName : "null"};
	}

	//If PGP
	if(optTestPageType.pageName === "Grid: " && jQuery('.product_listing_container .product_info .product_promo').length){
		jQuery('.product_image').on('click',function(){
			window.optimizely.push(["trackEvent", "PGPImageClick"]);
			window.optimizely.push(["trackEvent", "PGPClick"]);
		});
		jQuery('.product_name').on('click',function(){
			window.optimizely.push(["trackEvent", "PGPTitleClick"]);
			window.optimizely.push(["trackEvent", "PGPClick"]);
		});
		jQuery('.quick-view-btn').on('click',function(){
			window.optimizely.push(["trackEvent", "PGPQVClick"]);
		});
		jQuery(document).on('click','.qv-more-details-btn', function(){
			window.optimizely.push(["trackEvent", "PGPQVDetailsClick"]);
		});
	}

	//If PDP
	if(optTestPageType.pageCategory === "Product Page"){
		window.optimizely.push(["trackEvent", "PDPView"]);
		jQuery('.btn-add').on('click',function(){
			if(jQuery('#errorTooltip').css('display') === 'none'){
				window.optimizely.push(["trackEvent", "addedToCart"]);
			}
		});
	}

	//If Shopping Cart Page
	if(optTestPageType.pageCategory === "Shopping Cart"){
		window.optimizely.push(["trackEvent", "shoppingCartView"]);
	}

	//If Checkout Page
	if(optTestPageType.pageCategory === "CheckoutLoginFormView"){
		window.optimizely.push(["trackEvent", "checkoutView"]);
	}

})();

